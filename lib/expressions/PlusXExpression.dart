
import 'package:kiop2018/LTerm.dart';
import 'package:kiop2018/LType.dart';
import 'package:kiop2018/expressions/NumExpression.dart';
import 'package:kiop2018/types/FunctionType.dart';
import 'package:kiop2018/types/NumType.dart';
import 'package:kiop2018/utils/Environment.dart';

class PlusXExpression extends LTerm {
  int aNumber;

  PlusXExpression(int this.aNumber);

  @override
  LTerm applyTerm(LTerm rightTerm) {
    return NumExpression(aNumber + (rightTerm as NumExpression).value);
  }

  @override
  bool isReducible() {return false;}

  @override
  LTerm reduce() {throw "kajdhflaksjdfasdf";}

  @override
  LType typeOf(Environment e) {
    return new FunctionType(new NumType(), new NumType());
  }

  @override
  LTerm replaceVarWithTerm(String paramName, LTerm term) {
    return this;
  }
}


