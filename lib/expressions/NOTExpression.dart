import 'package:kiop2018/LTerm.dart';
import 'package:kiop2018/LType.dart';
import 'package:kiop2018/expressions/BoolExpression.dart';
import 'package:kiop2018/types/BoolType.dart';
import 'package:kiop2018/types/FunctionType.dart';
import 'package:kiop2018/utils/Environment.dart';

class NOTExpression extends LTerm {
  bool isReducible() {return false;}

  LTerm reduce() {throw "asjdhflkasjdfhasgd";}

  LType typeOf(Environment e) {return new FunctionType(new BoolType(), new BoolType());
  }

  @override
  LTerm applyTerm(LTerm rightTerm) {
    return new BoolExpression(!(rightTerm as BoolExpression).value);
  }

  @override
  LTerm replaceVarWithTerm(String paramName, LTerm term) {
    return this;
  }

}