import 'package:kiop2018/LTerm.dart';
import 'package:kiop2018/LType.dart';
import 'package:kiop2018/expressions/NumExpression.dart';
import 'package:kiop2018/expressions/PlusXExpression.dart';
import 'package:kiop2018/types/FunctionType.dart';
import 'package:kiop2018/types/NumType.dart';
import 'package:kiop2018/utils/Environment.dart';

class PlusExpression extends LTerm {
  bool isReducible() {return false;}
  LTerm reduce() {throw "geh mich wech";}
  LType typeOf(Environment e) {return new FunctionType(new NumType(), new FunctionType(new NumType(), new NumType()));}

  @override
  LTerm applyTerm(LTerm aTerm) {
    return new PlusXExpression((aTerm as NumExpression).value);
  }

  @override
  LTerm replaceVarWithTerm(String paramName, LTerm term) {
    return this;
  }
}
