import 'package:kiop2018/LTerm.dart';
import 'package:kiop2018/LType.dart';
import 'package:kiop2018/types/FunctionType.dart';
import 'package:kiop2018/utils/Environment.dart';


class Application extends LTerm {
  LTerm leftTerm;
  LTerm rightTerm;

  Application(this.leftTerm, this.rightTerm);

  /* via call by value */
  LTerm reduce() {
    if (leftTerm.isReducible()) {
      return new Application(leftTerm.reduce(), rightTerm);
    } else {
      if (rightTerm.isReducible()) {
        return new Application(leftTerm, rightTerm.reduce());
      } else {
        return leftTerm.applyTerm(rightTerm);
      }
    }
  }

  bool isReducible() {
    return true;
  }

  /*
                      E |= leftTerm: T1 -> resultType  E |= rightTerm: T1
      T-Abs        =======================================================
                          E |= leftTerm rightTerm: resultType
   */
  LType typeOf(Environment e) {
    FunctionType ft = leftTerm.typeOf(e);
    LType T1 = rightTerm.typeOf(e);

    if(T1!=ft.leftType) {
      throw "Type fehler:.....";
    } else {
      return ft.rightType;
    }
  }

  @override
  LTerm applyTerm(LTerm rightTerm) {
    // TODO: implement applyTerm
  }

  @override
  LTerm replaceVarWithTerm(String paramName, LTerm term) {
    return new Application(leftTerm.replaceVarWithTerm(paramName, term), rightTerm.replaceVarWithTerm(paramName, term));
  }



}
