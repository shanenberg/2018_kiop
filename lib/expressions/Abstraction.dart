
import 'package:kiop2018/LTerm.dart';
import 'package:kiop2018/LType.dart';
import 'package:kiop2018/types/FunctionType.dart';
import 'package:kiop2018/utils/Environment.dart';

class Abstraction extends LTerm {
  String paramName;
  LType paramType;
  LTerm body;

  Abstraction(this.paramName, this.paramType, this.body);


  @override
  bool isReducible() {
    return false;
  }

  @override
  LTerm reduce() {
    // TODO: implement reduce
  }

  /*
                         E, (paramName: paramType) |= body: bodyType
      T-Abs        =================================================
                   E |= λparamName:paramType.body: paramType->bodyType
   */
  LType typeOf(Environment e) {
    LType bodyType = body.typeOf(e.newWith(paramName, paramType));
    return new FunctionType(paramType, bodyType);
  }

  /*

      E-App        ===========================================================
                      (λparamName:paramType.body) t -> [paramName:=t]body
   */
  LTerm applyTerm(LTerm term) {
    return body.replaceVarWithTerm(paramName, term);
  }

  @override
  LTerm replaceVarWithTerm(String p, LTerm t) {
    return new Abstraction(paramName, paramType, body.replaceVarWithTerm(p, t));
  }

}
