import 'package:kiop2018/LTerm.dart';
import 'package:kiop2018/LType.dart';
import 'package:kiop2018/expressions/BoolExpression.dart';
import 'package:kiop2018/expressions/NumExpression.dart';
import 'package:kiop2018/expressions/PlusXExpression.dart';
import 'package:kiop2018/types/BoolType.dart';
import 'package:kiop2018/utils/Environment.dart';


class IfExpression extends LTerm {
  LTerm condition;
  LTerm thenTerm;
  LTerm elseTerm;

  IfExpression(this.condition, this.thenTerm, this.elseTerm);

  bool isReducible() {return true;}

  LTerm reduce() {
    if(condition.isReducible()) {
      return new IfExpression(condition.reduce(), thenTerm, elseTerm);
    } else {
      if ((condition as BoolExpression).value) {
        return thenTerm;
      } else {
        return elseTerm;
      }
    }
  }

  /*
                    E |= cond:BoolType E|= then: T   E|= else: T
      T-Abs        ==============================================
                   E |= if cond then t1 else t2= T
   */
  LType typeOf(Environment e) {
    if (!(condition.typeOf(e) is BoolType))
      throw "No Bool type in condition";

    LType T1 = thenTerm.typeOf(e);
    LType T2 = elseTerm.typeOf(e);

    if (T1 != T2)
      throw "invalid type: then and else should be the same type";

    return T1;

  }

  @override
  LTerm applyTerm(LTerm aTerm) {
    return new PlusXExpression((aTerm as NumExpression).value);
  }

  @override
  LTerm replaceVarWithTerm(String paramName, LTerm term) {
    return new IfExpression(
        condition.replaceVarWithTerm(paramName, term),
        thenTerm.replaceVarWithTerm(paramName, term),
        elseTerm.replaceVarWithTerm(paramName, term));
  }
}

