
import 'package:kiop2018/LTerm.dart';
import 'package:kiop2018/LType.dart';
import 'package:kiop2018/utils/Environment.dart';

class Variable extends LTerm {
  String varName;

  Variable(this.varName);

  @override
  bool isReducible() {return false;}

  @override
  LTerm reduce() {throw "aöklsjdfhöaksjdfh";}

/*
                             (varName: T) € E
      T-Var        =================================================
                             E |= varName: T
*/
  LType typeOf(Environment e) {
    if (!e.containsVariable(varName)) {
      throw "äölkälkjasdfousahdf";
    } else {
      return e.getTypeOfVarName(varName);
    }
  }

  @override
  LTerm applyTerm(LTerm rightTerm) {
    // TODO: implement applyTerm
  }

/*
    [x:=t] x -> t
    [x:=t] y -> t falls y != x
 */
  LTerm replaceVarWithTerm(String paramName, LTerm term) {
    if (!(this.varName==paramName))
      return this;
    else {
      return term;
    }
  }


}
