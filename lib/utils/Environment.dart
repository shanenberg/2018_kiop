
import 'dart:collection';

import 'package:kiop2018/LType.dart';

class Environment {
  Map<String, LType> map;

  Environment(this.map);

  Environment newWith(String varName, LType aType) {
    Map<String, LType> ret = new HashMap();
    ret.addAll(map);
    ret[varName] = aType;
    return new Environment(ret);
  }

  bool containsVariable(String varName) {
    return map.containsKey(varName);
  }

  LType getTypeOfVarName(String varName) {
    return map[varName];
  }
}

