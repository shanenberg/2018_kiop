
import 'package:kiop2018/LType.dart';

class FunctionType extends LType {
  LType leftType;
  LType rightType;

  FunctionType(this.leftType, this.rightType);

  bool operator ==(Object o) {
    if (!(o is FunctionType)) {
      return false;
    } else {
      return (o as FunctionType).leftType==leftType && (o as FunctionType).rightType == rightType;
    }
  }
}