
import 'package:kiop2018/LType.dart';

class BoolType extends LType {
  bool operator ==(Object o) {
    return o is BoolType;
  }
}
