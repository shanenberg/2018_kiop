import 'package:kiop2018/LType.dart';
import 'package:kiop2018/utils/Environment.dart';

abstract class LTerm {
  LTerm reduce();
  bool isReducible();
  LType typeOf(Environment e);

  LTerm applyTerm(LTerm rightTerm);

  LTerm replaceVarWithTerm(String paramName, LTerm term);
}
