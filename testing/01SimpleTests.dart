import 'dart:collection';

import 'package:kiop2018/LTerm.dart';
import 'package:kiop2018/LType.dart';
import 'package:kiop2018/expressions/Abstraction.dart';
import 'package:kiop2018/expressions/Application.dart';
import 'package:kiop2018/expressions/BoolExpression.dart';
import 'package:kiop2018/expressions/IfExpression.dart';
import 'package:kiop2018/expressions/NOTExpression.dart';
import 'package:kiop2018/expressions/NumExpression.dart';
import 'package:kiop2018/expressions/PlusExpression.dart';
import 'package:kiop2018/expressions/Variable.dart';
import 'package:kiop2018/types/BoolType.dart';
import 'package:kiop2018/types/FunctionType.dart';
import 'package:kiop2018/types/NumType.dart';
import 'package:kiop2018/utils/Environment.dart';
import 'package:test/test.dart';


main() {


  test("Damn simple Tests", () {

    Environment e;
    BoolExpression aTrue;
    BoolExpression aFalse;
    Application anApplication;
    Abstraction anAbstraction;
    LTerm aResult;
    NumExpression aNum;
    NumExpression anotherNum;
    IfExpression anIf;

    //  (x:NumType) |= x:NumType
    Variable x = new Variable("x");
    e = new Environment(new HashMap());
    Environment e1 = e.newWith("x", new NumType());
    expect(x.typeOf(e1) is NumType, true);

    //  1:NumType
    NumExpression one = new NumExpression(1);
    e = new Environment(new HashMap());
    expect(one.typeOf(e) is NumType, true);

    //  true:BoolType
    aTrue = new BoolExpression(true);
    e = new Environment(new HashMap());
    expect(aTrue.typeOf(e) is BoolType, true);

    //  false:BoolType
    aFalse = new BoolExpression(false);
    e = new Environment(new HashMap());
    expect(aTrue.typeOf(e) is BoolType, true);

    //  false:BoolType
    aFalse = new BoolExpression(false);
    e = new Environment(new HashMap());
    expect(aTrue.typeOf(e) is BoolType, true);

    //  NOT false -> true;
    aFalse = new BoolExpression(false);
    e = new Environment(new HashMap());
    anApplication = new Application(new NOTExpression(), aFalse);
    aResult = anApplication.reduce();
    expect(aResult.typeOf(e) is BoolType, true);
    expect((aResult as BoolExpression).value, true);

    //  + 1 2 -> ... -> 3;
    aNum = new NumExpression(1);
    anotherNum = new NumExpression(2);
    e = new Environment(new HashMap());
    anApplication = new Application(new Application(new PlusExpression(), aNum), anotherNum);
    aResult = anApplication.reduce();
    aResult = aResult.reduce();
    expect((aResult as NumExpression).value, 3);

    //  λx:NumType.x: NumType->NumType
      Abstraction a = new Abstraction("n", new NumType(), new Variable("n"));
      e = new Environment(new HashMap());
      LType t = a.typeOf(e);
      expect(t is FunctionType, true);
      expect((t as FunctionType).leftType  is NumType, true);
      expect((t as FunctionType).rightType is NumType, true);

    //  if(not true) then 666 else 42;
    anIf = new IfExpression(new Application(new NOTExpression(), new BoolExpression(true)), new NumExpression(666), new NumExpression(42));
    e = new Environment(new HashMap());
    expect((anIf.typeOf(e) is NumType), true);

    aResult = anIf.reduce().reduce();
    expect((aResult as NumExpression).value, 42);

    //  (λx:NumType.x) 42: NumType
    anApplication = new Application(
        new Abstraction("n", new NumType(), new Variable("n")),
        new NumExpression(42)
    );

    e = new Environment(new HashMap());
    expect(anApplication.typeOf(e) is NumType, true);

    //  (λx:NumType.x) 42 -> 42
    expect((anApplication.reduce() as NumExpression).value, 42);


  });

  test("Damn simple Tests", ()
  {
 /*   NOTExpression notExpression = new NOTExpression();
    TrueExpression trueExp = new TrueExpression();

    Application a = new Application(notExpression, trueExp);
*/  });


}
